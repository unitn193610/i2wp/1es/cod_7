package cod_7;
//Copia del codice visto in cod_6 ma ampliato con quanto richiesto nella consegna.
/*
Reading the whole content of a file and allocating it in a single array can become a problem
if the file is large. Try to optimise the code by using buffers to stream the data to the client.
 */
import java.io.*;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.NoSuchFileException;
import java.util.StringTokenizer;

public class TinyHttpdConnection extends Thread {

    private final Socket SOCKET;

    public TinyHttpdConnection(Socket s)
    {
        this.SOCKET = s;

        setPriority(NORM_PRIORITY - 1);

        initInstance();
    }

    private void initInstance(){
        start(); //facciamo partire il Thread
    }

    @Override
    public void run()
    {
        try {
            //--------------------------------------------------------------------------------
            BufferedReader in = new BufferedReader(new InputStreamReader(SOCKET.getInputStream()));

            OutputStream out = SOCKET.getOutputStream();

            String req = in.readLine(); //req è la stringa in cui andremmo a porre l'intera stringa digitata nell'url
            if(req == null)
                req=""; //Così preveniamo gli errori di NullPointerException
            System.out.println("Request: " + req);

            StringTokenizer st = new StringTokenizer(req);

            //Estraiamo ora il verbo GET ed almeno un nome o un path
            if((st.countTokens() >= 2) && st.nextToken().equals("GET"))
            {
                if((req = st.nextToken()).startsWith("/"))
                    req = req.substring(1); //req=index.html

                if(req.endsWith("/") || req.isEmpty())
                    req = req + "null.html";

                try {
                    /*
                    Questo è il modo più semplice per file di piccola dimensione dal momento
                    che alloca la memoria necessaria in ByteBuffer tramite la dimensione FileChannel
                     */

                    FileInputStream file_in = new FileInputStream(req);
                    FileChannel channel = file_in.getChannel();
                    ByteBuffer buffer = ByteBuffer.allocate((int)channel.size());
                    channel.read(buffer);
                    buffer.flip();

                    out.write("HTTP/1.1 200 OK\r\n".getBytes());
                    out.write("Content-Type: text/html; charset=utf-8\r\n".getBytes());
                    out.write("\r\n".getBytes());

                    try {
                        for(int i=0; i<channel.size(); i++) {
                            out.write((char) buffer.get());
                        }
                    }
                    catch(IOException e){
                        new PrintStream(out).println("Errore di lettura: " + e.getMessage());
                    }

                    try{
                        channel.close();
                        file_in.close();
                    }
                    catch(Exception e){
                        System.err.println("Errora durante la chiusura del file: " + e.getMessage());
                        e.printStackTrace(System.err);
                    }

                }
                catch (IOException e) {
                    if (e instanceof NoSuchFileException) {
                        //Se il file non è stato trovato
                        out.write("HTTP/1.1 404 Not Found\r\n".getBytes());
                        out.write("\r\n".getBytes()); //termino l'header, in questo caso composto solo dalla stringa qui sopra
                        out.write("404 FILE NOT FOUND".getBytes()); //stampa a video nel browser
                    } else {
                        out.write("HTTP/1.1 500 Internal error: \r\n ".getBytes()); //inizio e termine primo header
                        out.write(e.getMessage().getBytes()); // secondo header che può essere visualizzato dalla console del browser
                        out.write("\r\n".getBytes()); //termine secondo header
                        out.write("500 Internal error".getBytes()); //stampa a video nel browser

                        new PrintStream(out).println("HTTP/1.1 500 Internal error: " + e.getMessage());
                    }
                }
            }

            else
            {
                /*
                Per ora, in TinyHttpd ci limiato ad accettare solo stringhe
                del tipo: GET /path/to/file
                (quindi niente POST,PUT,DELETE, etc...)
                 */
                out.write("HTTP/1.1 400 Bad request\r\n".getBytes());

                new PrintStream(out).println("Metodo ancora non supportato."); //stampa a video nel browser
            }
            //----------------------------------------------------------------------------------
        }
        catch(IOException e){
            System.err.println("Errore durante la lettura della richiesta: " + e.getMessage());
            e.printStackTrace(System.err);
        }
        finally{
            try{
                SOCKET.close();
            }
            catch(IOException e){
                System.err.println("Errore durante la chisura della socket: " + e.getMessage());
                e.printStackTrace(System.err);
                System.exit(1);
            }
        }
    }
}