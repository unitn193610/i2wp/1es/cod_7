package cod_7;
/*
Questo codice è identico a quello visto nella directory cod_5 (HttpdGet.java).
Ricordiamoci dunque di passare come parametro nelle impostazione di Run dell'IDE
una porta valida (es. 3000).
 */

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.UnknownHostException;

public class TinyHttpd { //funge da mini-server

    public static void main(String[] args)
    {
        if(args.length != 1){
            System.out.println("Sintassi: HttpdGet <porta>");
            System.exit(1);
        }

        ServerSocket server = null;

        try{
            server = new ServerSocket(Integer.parseInt(args[0]));
        }
        catch(IOException e){
            System.err.println("Errore durante la creazione di un server socket: " + e.getMessage());
            e.printStackTrace(System.err);
            System.exit(1);
        }
        catch(NumberFormatException e){
            System.err.println("Il parametro dell'argomento non è un numero valido per la porta. " + e.getMessage());
            e.printStackTrace(System.err);
            System.out.println("Sintassi: HttpdGet <porta>");

            System.exit(0);
        }

        try{
            InetAddress serverInfo = InetAddress.getLocalHost();
            System.out.println("Host info:");
            System.out.println(serverInfo.toString());
            System.out.println("PORT: " + server.getLocalPort());

            System.out.println(
                    "Richiedi un file all'url: " +
                            serverInfo.getHostAddress() +
                            ":" +
                            server.getLocalPort()
            + "\n");

            while(true){
                new TinyHttpdConnection(server.accept());
            }
        }
        catch(UnknownHostException e){
            System.err.println("Errore durante la lettura dell'host-name: " + e.getMessage());
            e.printStackTrace(System.err);
        }
        catch(IOException e){
            System.err.println("Errore durante l'accettazione delle richieste: " + e.getMessage());
            e.printStackTrace(System.err);
        }
    }
}

/*
Ricorda di porre nelle impostazioni di Run dell'IDE
come working directory la cartella dove si trova il presente codice.
Per semplicità porre nella stessa anche i file che vorremmo visualizzare
sul browser, ad esempio l'index.html.

Esecuzione:
- Run
- apri browser e collegati all'url: indirizzo:porta
esempio: 192.168.1.60:3000/index.html
 */