# HOMEWORK 1

Reading the whole content of a file and allocating it in a single array can become a problem if the file is large. Try to optimise the code by using buffers to stream the data to the client.
Make sure read the file one line at time. Send the line to the socket channel.